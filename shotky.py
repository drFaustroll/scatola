#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

__author__ = "Alin Marin Elena <alin@elena.space>"
__copyright__ = "Copyright© 2018-2019 Alin M Elena"
__license__ = "GPL-3.0-only"
__version__ = "1.0"

import random
import argparse as cli

def readConfig(filename,at,cell):
   with open(filename) as f:
       title=f.readline()
       lvl,imcon,n= (int(i) for i in f.readline().strip().split())
       for i in range(3):
           l=f.readline().strip().split()
           cell.append([float(l[0]),float(l[1]),float(l[2])])
       for i in range(n):
           nm=f.readline().strip().split()[0]
           r=f.readline().strip().split()
           at.append([nm,float(r[0]),float(r[1]),float(r[2])])
       return lvl,imcon,len(at)

def vacancies(at,v,i):
    n=len(at)-1
    k=0

    for j in sorted(random.sample(range(n//3),v),reverse=True):
        if (k<i):
           at[3*j]=['He',at[3*j][1],at[3*j][2],at[3*j][3]]
           at[3*j+1]=['He',at[3*j+1][1],at[3*j+1][2],at[3*j+1][3]]
           at[3*j+2]=['He',at[3*j+2][1],at[3*j+2][2],at[3*j+2][3]]
           at[n-3*k],at[3*j+2] = at[3*j+2],  at[n-3*k]
           at[n-3*k-1],at[3*j+1] = at[3*j+1],  at[n-3*k-1]
           at[n-3*k-2],at[3*j] = at[3*j],  at[n-3*k-2]
        else:
           del at[3*j+2]
           del at[3*j+1]
           del at[3*j]
        k=k+1   

def writeConfig(nconfig,ats,cell,lvl,imcon):
    n=len(ats)
    f=open(nconfig,"w")
    f.write("{0:72s}\n".format("PuO2 with defects"))
    f.write("{0:10d}{1:10d}{2:10d}{3:42s}\n".format(lvl,imcon,n,' '))
    f.write('{0:20.10f}{1:20.10f}{2:20.10f}{3:12s}\n'.format(*cell[0],' '))
    f.write('{0:20.10f}{1:20.10f}{2:20.10f}{3:12s}\n'.format(*cell[1],' '))
    f.write('{0:20.10f}{1:20.10f}{2:20.10f}{3:12s}\n'.format(*cell[2],' '))
    for i in range(n):
        f.write("{0:8s}{1:10d}{2:54s}\n{3:20.10f}{4:20.10f}{5:20.10f}{6:12s}\n".format(
            ats[i][0], i+1,' ', ats[i][1], ats[i][2], ats[i][3],' '))
    f.close()

def writeFIELD(field,n,i):
    f=open(field,"w")
    f.write("{0:72s}\n".format("PuO2 with He using Arima et al. and Grimes et al. potentials"))
    f.write("{0:72s}\n".format("units eV"))
    if (i>0):
       f.write("molecular types {0:d}\n".format(2))
    else:
       f.write("molecular types {0:d}\n".format(1))
    f.write("{0:72s}\n".format("PuO2"))
    f.write("nummols {0:d}\n".format((n-3*i)//3))
    f.write("{0:72s}\n".format("atoms 3"))
    f.write("{0:72s}\n".format("Pu          239.0000   2.70    1    0"))
    f.write("{0:72s}\n".format("O            16.0000   -1.35   2    0"))
    f.write("{0:72s}\n".format("finish"))
    if (i>0):
       f.write("{0:72s}\n".format("He"))
       f.write("nummols {0:d}\n".format(i))
       f.write("{0:72s}\n".format("atoms 3"))
       f.write("{0:72s}\n".format("He            4.0000   0.00   3   0"))
       f.write("{0:72s}\n".format("finish"))
    if (i>0):
       f.write("vdw {0:d}\n".format(6))
    else:
       f.write("vdw {0:d}\n".format(3))
    f.write("{0:72s}\n".format("Pu Pu buck 280459823100000 0.065 0.0"))
    f.write("{0:72s}\n".format("Pu O buck 57424.5891 0.1985 0.0"))
    f.write("{0:72s}\n".format("O O buck 978.7081 0.332 17.3542"))
    if (i>0):
       f.write("{0:72s}\n".format("He He 12-6 69.3559 0.493712"))
       f.write("{0:72s}\n".format("He O 12-6 2247.836 11.762"))
       f.write("{0:72s}\n".format("He Pu 12-6 500.425 7.366"))
    f.write("{0:72s}\n".format("close"))
    f.close()

def set_cli():
  parser = cli.ArgumentParser(description='simple code to create shotky impurities and fill some with He in PuO2')
  parser.add_argument('--config', help='config file to read from (default: %(default)s)',default="PuO2.CONFIG")
  parser.add_argument('--newconfig', help='config file name to write (default: %(default)s)',default="PuO2.newconfig")
  parser.add_argument('--field', help='field file name to write (default: %(default)s)',default="PuO2.field")
  parser.add_argument('--vacancies',type=int, help='number of shotky vacancies to be created (default: %(default)s)',default=5)
  parser.add_argument('--impurities',type=int, help='number of impurities to be added *3 (default: %(default)s)',default=5)
  parser.add_argument('--seed',type=int, help='seed for random numbers (default: %(default)s)',default=42)
  return parser.parse_args()

if __name__ == '__main__':

  cli = set_cli()

  vac = cli.vacancies
  he = cli.impurities

  if (he != 0):
    print("Creating impurities for {0:d} sites\n".format(he))
  if (vac != 0):
    print("Creating vacancies for {0:d} sites\n".format(vac))

  if (he == 0 and vac == 0):
    print("Nothing to do!!! Try to create a vacancy or impurity at least")
    sys.exit()

  if (he>0 and vac==0):
    vac = he
  if (he > vac):
    print("you cannot create more impurities than vacancies")
    sys.exit()

  atoms=[]
  cell=[]
  lvl,imcon,n=readConfig(cli.config,atoms,cell)
  print("Read {0:d} atoms from file, neutral groups {1:d}".format(n,n//3))
  random.seed(cli.seed)
  vacancies(atoms,vac,he)
  print("write {0:d} atoms to {1:s}".format(len(atoms),cli.newconfig))
  writeConfig(cli.newconfig,atoms,cell,lvl,imcon)
  print("write  field file to {0:s}".format(cli.field))
  writeFIELD(cli.field,len(atoms),he)
