#!/usr/bin/env python3 
# -*- coding: UTF-8 -*-

__author__ = "Alin Marin Elena <alin@elena.space>"
__copyright__ = "Copyright© 2018 Alin M Elena"
__license__ = "GPL-3.0-only"
__version__ = "1.0"

import sys
from PyQt5.QtWidgets import QWidget,QApplication,QMainWindow,QLineEdit,\
        QLabel,QAction,QVBoxLayout,QPushButton,\
        QHBoxLayout,QGroupBox,QFormLayout,QComboBox,\
        QCheckBox,QPlainTextEdit,QMessageBox

from PyQt5.QtGui import QIcon,QIntValidator,QDoubleValidator
from PyQt5.QtCore import pyqtSlot,QProcess

class App(QMainWindow):
  def __init__(self):
    super().__init__()
    self.title="DL_POLY control flow file generator"
    self.left=10
    self.top=10
    self.width=1200
    self.height=800
    self.InitUI()

  def InitUI(self):
    self.setWindowTitle(self.title)
    self.setGeometry(self.left,self.top,self.width,self.height)
    self.statusBar().showMessage("earth, milky way")
    
    self.main = MyControl(self)
    self.setCentralWidget(self.main)
    self.createActions()
    self.main.layout.addWidget(self.act)

    menu = self.menuBar()
    fileMenu = menu.addMenu('File')
    editMenu = menu.addMenu('Edit')
    viewMenu = menu.addMenu('View')
    toolMenu = menu.addMenu('Tools')
    helpMenu = menu.addMenu('Help')

#  file menu buttons
    exit = QAction(QIcon('exit.png'), 'Exit', self)
    exit.setShortcut('Ctrl+Q')
    exit.setStatusTip('Exit application')
    exit.triggered.connect(self.close)
    fileMenu.addAction(exit)
    
    self.show()

  def createActions(self):
    self.act = QGroupBox('')
    layout = QHBoxLayout()

    self.b_run = QPushButton("Run")
    self.b_run.clicked.connect(self.on_run)

    self.b_cancel = QPushButton("Cancel")
    self.b_cancel.setEnabled(False)
    self.b_cancel.clicked.connect(self.on_cancel)

    b_exit = QPushButton("Exit")
    b_exit.clicked.connect(self.close)

    layout.addWidget(self.b_run)
    layout.addWidget(self.b_cancel)
    layout.addWidget(b_exit)
    self.act.setLayout(layout)

  @pyqtSlot()
  def on_run(self):
    self.main.readForm()
    cmd="./DLPOLY.Z"
    self.p=QProcess(self)
    self.p.start(cmd)
    self.p.stateChanged.connect(self.on_running)
    self.p.errorOccurred.connect(self.on_error)


  def on_error(self,e):
    err = QMessageBox()
    err.setIcon(QMessageBox.Critical)
    err.addButton("OK",QMessageBox.RejectRole)
    if e==0:
        errm="Failed to start"
    elif e == 1:
        errm="Crashed or killed"
    elif e > 1:
        errm="Other errors, check OUTPUT"
    err.setText("error occured:\n {0:s}".format(errm))
    err.exec()

  def on_running(self,s):
    if s == 0:
      self.b_run.setText("Run")
      self.b_cancel.setEnabled(False)
    elif s==1:
      self.b_run.setText("Starting")
      self.b_cancel.setEnabled(True)
    elif s==2:
      self.b_run.setText("Running")
      self.b_cancel.setEnabled(True)

  
  @pyqtSlot()
  def on_cancel(self):
     self.p.kill()
     self.b_cancel.setEnabled(False)
     self.b_run.setText("Run")
  

class MyControl(QWidget):
  def __init__(self,parent):
    super(QWidget, self).__init__(parent)

    self.layout = QVBoxLayout()
    self.createFormControl()

    self.layout.addStretch(1)
    self.layout.addWidget(self.control)
    self.setLayout(self.layout)


  def readForm(self):
     lines = "{0:72s}\n".format(self.title.text())
     lines+="temperature {0:12.4f}\n".format(float(self.temperature.text()))
     lines+="pressure {0:12.4f}\n".format(float(self.pressure.text()))
     lines+="steps {0:12d}\n".format(int(self.steps.text()))
     lines+="equilibration steps {0:12d}\n".format(int(self.equil.text()))
     lines+="scale {0:12d}\n".format(int(self.scale.text()))
     t="timestep {0:12.6f}\n".format(float(self.timestep.text()))
     if self.variable.isChecked():
         lines+= "variable "+t
     else:
         lines+=t
     lines+="cutoff {0:12.4f}\n".format(float(self.cutoff.text()))
     if self.electrostatics.isChecked():
        lines+="ewald precision {0:12.5e}\n".format(float(self.ewaldp.text()))
     else:
        lines+="no elec\n"

     en=self.ensemble.currentText()
     ens="ensemble "+en
     fl=self.flavour.currentText()
     if fl is not '':
         ens +=" "+fl
     if self.ensf1.isEnabled():
         ens +=" {0:12.6f}".format(float(self.ensf1.text()))
     if self.ensf2.isEnabled():
         ens +=" {0:12.6f}".format(float(self.ensf2.text()))
     if self.nstf.isEnabled():
         nf=self.nstf.currentText()
         if nf != 'none':
           ens +=" {0:s}".format(nf)
     if self.ensg.isEnabled():
         ens +=" {0:12.6f}".format(float(self.ensg.text()))
     if self.semi.isEnabled():
         if self.semi.isChecked():
            ens +=" semi"
           
     lines+=ens+"\n"
     lines+="print every {0:12d}\n".format(int(self.printe.text()))
     lines+="stats every {0:12d}\n".format(int(self.stats.text()))
     lines+="collect\n"
     if self.trajectory.isChecked():
         lines+="traj {0:12d} {1:12d} {2:12d}\n".format(int(self.traj_start.text()),\
                 int(self.traj_freq.text()),self.traj_detail.currentIndex())
     if self.rdf.isChecked():
         lines+="rdf {0:12d}\nbinsize {0:12.6f}\nprint rdf\n".format(int(self.rdf_freq.text()),\
                 float(self.rdf_binsize.text()))
     if self.adv.isChecked():
         lines+=self.advArea.toPlainText()+"\n"
     lines+="job time 10000000\n"
     lines+="close time 1000\n"
     lines+="finish\n"

     with open("CONTROL","w") as f:
       f.write(lines)

  def createFormControl(self):

    layout = QFormLayout()
    self.control = QGroupBox("DL_POLY: Control")
    
    self.title = QLineEdit()
    self.title.setText('This is a title')
    layout.addRow(QLabel('Title'),self.title)
    
    self.temperature = QLineEdit()
    self.temperature.setValidator(QDoubleValidator())
    self.temperature.setText('300.0')
    layout.addRow(QLabel('Temperature'),self.temperature)
    
    self.pressure = QLineEdit()
    self.pressure.setValidator(QDoubleValidator())
    self.pressure.setText('0.0')
    layout.addRow(QLabel('Pressure'),self.pressure)
    
    self.steps = QLineEdit()
    self.steps.setValidator(QIntValidator())
    self.steps.setText('2000')
    layout.addRow(QLabel('Simulation Steps'),self.steps)

    self.equil = QLineEdit()
    self.equil.setValidator(QIntValidator())
    self.equil.setText('1000')
    layout.addRow(QLabel('Equilibration Steps'),self.equil)
    
    self.scale = QLineEdit()
    self.scale.setValidator(QIntValidator())
    self.scale.setText('5')
    layout.addRow(QLabel('Rescale velocities every: '),self.scale)
    
    self.timestep = QLineEdit()
    self.timestep.setValidator(QDoubleValidator())
    self.timestep.setText('0.001')
    self.variable = QCheckBox()
    layh=QHBoxLayout()
    layh.addWidget(self.timestep)
    layh.addWidget(QLabel('Variable?'))
    layh.addWidget(self.variable)
    layh.addStretch()
    layout.addRow(QLabel('Timestep'),layh)

    self.cutoff = QLineEdit()
    self.cutoff.setValidator(QDoubleValidator())
    self.cutoff.setText('10.0')
    layout.addRow(QLabel('Cutoff'),self.cutoff)

    self.electrostatics = QCheckBox()
    layout.addRow(QLabel('Electrostatics? '),self.electrostatics)
    self.electrostatics.stateChanged.connect(self.do_elec)

    self.ewaldp = QLineEdit()
    self.ewaldp.setValidator(QDoubleValidator())
    self.ewaldp.setText('1.0e-6')
    self.ewaldp.setEnabled(False)
    layout.addRow(QLabel('Ewald precision'),self.ewaldp)
    self.ens ={'nve':[],'nvt':['evans','langevin','andersen','berendesen','hoover','gst','dpds1','dpds2'],\
            'npt':['langevin','berendesen','hoover','mtk'],\
            'nst':['langevin','berendesen','hoover','mtk']}
    self.ensnst=['none','area','tens','orth']
    
    self.opt_ens=QGroupBox("Ensemble: ")
    self.opt_ens.layout=QHBoxLayout()
    self.opt_ens.layout.addStretch(1)
    self.ensemble=QComboBox()
    self.ensemble.addItems(self.ens.keys())
    self.opt_ens.layout.addWidget(self.ensemble)
    self.flavour=QComboBox()
    self.flavour.addItems(self.ens['nve'])
    self.opt_ens.layout.addWidget(QLabel('flavour'))
    self.opt_ens.layout.addWidget(self.flavour)

    self.ensf1 = QLineEdit()
    self.ensf1.setValidator(QDoubleValidator())
    self.ensf1.setText('0.5')
    self.ensf1.setEnabled(False)
    self.opt_ens.layout.addWidget(QLabel('f1'))
    self.opt_ens.layout.addWidget(self.ensf1)

    self.ensf2 = QLineEdit()
    self.ensf2.setValidator(QDoubleValidator())
    self.ensf2.setText('0.2')
    self.ensf2.setEnabled(False)
    self.opt_ens.layout.addWidget(QLabel('f2'))
    self.opt_ens.layout.addWidget(self.ensf2)

    self.nstf=QComboBox()
    self.nstf.addItems(self.ensnst)
    self.nstf.setEnabled(False)
    self.opt_ens.layout.addWidget(QLabel('nst'))
    self.opt_ens.layout.addWidget(self.nstf)

    self.ensg = QLineEdit()
    self.ensg.setValidator(QDoubleValidator())
    self.ensg.setText('0.2')
    self.ensg.setEnabled(False)
    self.opt_ens.layout.addWidget(QLabel('γ'))
    self.opt_ens.layout.addWidget(self.ensg)

    self.semi = QCheckBox()
    self.semi.setEnabled(False)
    self.opt_ens.layout.addWidget(QLabel('semi'))
    self.opt_ens.layout.addWidget(self.semi)

    self.ensemble.currentIndexChanged.connect(self.do_ensemble_flavour)
    self.flavour.currentIndexChanged.connect(self.do_ensemble_opts)
    self.nstf.currentIndexChanged.connect(self.do_nst_opts)

    self.opt_ens.setLayout(self.opt_ens.layout)
    layout.addRow(self.opt_ens)
    

    self.printe = QLineEdit()
    self.printe.setValidator(QIntValidator())
    self.printe.setText('10')
    layout.addRow(QLabel('Print every '),self.printe)

    self.stats = QLineEdit()
    self.stats.setValidator(QIntValidator())
    self.stats.setText('10')
    layout.addRow(QLabel('Stats every '),self.stats)

    self.trajectory = QCheckBox()
    layout.addRow(QLabel('Trajectory '),self.trajectory)
    self.trajectory.stateChanged.connect(self.do_traj)
    self.opt_traj=QGroupBox("options: ")
    self.opt_traj.setEnabled(False)
    self.opt_traj.layout=QHBoxLayout()
    self.opt_traj.layout.addStretch(1)
    self.opt_traj.layout.addWidget(QLabel("start"))
    self.traj_start=QLineEdit()
    self.traj_start.setValidator(QIntValidator())
    self.traj_start.setText('10')
    self.opt_traj.layout.addWidget(self.traj_start)

    self.opt_traj.layout.addWidget(QLabel("freq"))
    self.traj_freq=QLineEdit()
    self.traj_freq.setValidator(QIntValidator())
    self.traj_freq.setText('10')
    self.opt_traj.layout.addWidget(self.traj_freq)
    self.opt_traj.layout.addWidget(QLabel("Detail: "))
    self.traj_detail=QComboBox()
    self.traj_detail.addItems(['coordinates','velocities','forces'])

    self.opt_traj.layout.addWidget(self.traj_detail)
    
    self.opt_traj.setLayout(self.opt_traj.layout)
    layout.addRow(QLabel(),self.opt_traj)

    self.rdf = QCheckBox()
    layout.addRow(QLabel('RDF '),self.rdf)
    self.rdf.stateChanged.connect(self.do_rdf)

    self.rdf_binsize = QLineEdit()
    self.rdf_binsize.setValidator(QDoubleValidator())
    self.rdf_binsize.setText('0.1')
    self.rdf_binsize.setEnabled(False)
    layout.addRow(QLabel('Binsize'),self.rdf_binsize)

    self.rdf_freq = QLineEdit()
    self.rdf_freq.setValidator(QIntValidator())
    self.rdf_freq.setText('10')
    self.rdf_freq.setEnabled(False)
    layout.addRow(QLabel('RDF every '),self.rdf_freq)

    self.adv = QCheckBox()
    layout.addRow(QLabel('Advanced '),self.adv)
    self.adv.stateChanged.connect(self.do_advanced)

    self.advArea=QPlainTextEdit()
    self.advArea.setEnabled(False)
    layout.addRow(QLabel(),self.advArea)

    self.control.setLayout(layout)


  def do_ensemble_opts(self,c):
     self.ensf1.setEnabled(False)
     self.ensf2.setEnabled(False)
     self.ensg.setEnabled(False)
     ens=self.ensemble.currentText()
     flav=self.flavour.currentText()
     if ens == 'nvt':
         if flav in ['langevin','berendesen','hoover']:
            self.ensf1.setEnabled(True)
         elif flav in ['andersen','gst']:   
            self.ensf1.setEnabled(True)
            self.ensf2.setEnabled(True)
         elif flav in ['dpds1','dpds2']:   
            self.ensg.setEnabled(True)
     elif ens == 'npt':
         self.ensf1.setEnabled(True)
         self.ensf2.setEnabled(True)
     elif ens == 'nst':
         self.ensf1.setEnabled(True)
         self.ensf2.setEnabled(True)
         self.nstf.setEnabled(True)
            
  def do_nst_opts(self,c):
      self.ensg.setEnabled(False)
      self.semi.setEnabled(False)
      nstf=self.nstf.currentText()
      if nstf == 'tens':
        self.ensg.setEnabled(True)
        self.semi.setEnabled(True)
      elif nstf == 'orth':
        self.semi.setEnabled(True)
          

  def do_elec(self):
      if self.electrostatics.isChecked():
          self.ewaldp.setEnabled(True)
      else:
          self.ewaldp.setEnabled(False)

  def do_traj(self):
      if self.trajectory.isChecked():
          self.opt_traj.setEnabled(True)
      else:
          self.opt_traj.setEnabled(False)

  def do_rdf(self):
      if self.rdf.isChecked():
          self.rdf_binsize.setEnabled(True)
          self.rdf_freq.setEnabled(True)
      else:
          self.rdf_binsize.setEnabled(False)
          self.rdf_freq.setEnabled(False)

  def do_ensemble_flavour(self,c):
      self.flavour.clear()
      key=self.ensemble.currentText()
      self.flavour.addItems(self.ens[key])

  def do_advanced(self):
      if self.adv.isChecked():
          self.advArea.setEnabled(True)
      else:
          self.advArea.setEnabled(False)

if __name__ == '__main__':
  app = QApplication(sys.argv)
  exe = App()
  sys.exit(app.exec_())

