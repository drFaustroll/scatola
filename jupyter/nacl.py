#!/usr/bin/env python3

import dlpoly as dlp

dlp.control.set(io=dict(field="nacl.field",config="nacl.config"),temperature=300.0,steps=10,equilibration=5,print=1,stats=1)
print(dlp.control.d)
dlp.control.write(filename="mycontrol")
dlp.run("~/playground/dlpoly/dl-poly-alin/build-mpi/bin/DLPOLY.Z", np=2,modules="gnu openmpi",control="mycontrol")
