#!/usr/bin/env python3

from copy import deepcopy as copy
import collections
import six

# python 3.8+ compatibility
try:
    collectionsAbc = collections.abc
except:
    collectionsAbc = collections


dlp_tokens = dict(temperature = 100.0,finish = 'finish',title='no title',io={},ensemble={},pressure=0.0,collect=False,\
        steps=10,equilibration=5,print=1,stats=1,cutoff=0.0,variable=False,timestep=0.001,job=1000,close=100)



def update(d, u):
  for k, v in six.iteritems(u):
    dv = d.get(k, {})
    if not isinstance(dv, collectionsAbc.Mapping):
        d[k] = v
    elif isinstance(v, collectionsAbc.Mapping):
        d[k] = update(dv, v)
    else:
        d[k] = v
  return d

def checktokens(x):
  for key, val in x.items():
    if key in dlp_tokens:
      assert type(val) == type(dlp_tokens[key]), key + " has the wrong type"
    else:
        print("key {:s} is unknown check the manual".format(key))

class control(object):

    def __init__(self, **d):
      self.d={}
      self.d['finish']='finish'
      self.d['io']=dict(field='FIELD',config='CONFIG',outstats='STATIS')
      self.d['title']='no title'
      self.d['ensemble']=dict(type='nve',flavour='langevin',aniso='',f=0.5,f1=0.5,f2=0.5,f3=0.5,gamma=0.5,semi=False)
      self.d['temperature']=300.0
      self.d['pressure']=0.0
      self.d['cutoff']=8.0
      self.d['timestep']=0.001
      self.d['job']=1000
      self.d['close']=100
      self.d['variable']=False

      update(self.d,d)
      checktokens(d)

    def set(self, **nd):
       update(self.d,nd)
       checktokens(nd)

    def directives(self):
      return self.d.keys()

    def adddire(self,key,nokey=False):
      if nokey:
        line = "{0:s}\n".format(str(self.d[key]))
      else:
        line = "{0:s} {1:}\n".format(key,self.d[key])
      return line


    def addio(self,v):
        lines=""
        for key,val in v.items():
            lines+="io {0:s} {1:}\n".format(key,val)
        return lines

    def addtimestep(self,v):

        lines = "timestep {0:}\n".format(v)
        if self.d['variable']:
            lines = "variable "+lines
        return lines

    def addensemble(self,v):
      types=['nve','nvt','npt','nst']
      flavour={'nvt':['evans','berendsen','langevin','andersen','hoover','gst','ttm','inhomogenous','dpd'],\
           'npt':['langevin','berendsen','hoover','mtk'],\
           'nst':['langevin','berendsen','hoover','mtk']}
      vt=v['type']
      aniso=['area','tens','orth','']

      lines="ensemble"
      if vt not in types:
          print("Unsupported thermostat type, {0:}, read the manual".format(vt))
      else:
        if vt is 'nve':
          lines += " {0:}".format(vt)
        elif vt is 'nvt' and (v['flavour'] in flavour['nvt']):
          lines += " {0:} {1:}".format(vt,v['flavour'])
        elif vt is 'npt' and v['flavour'] in flavour['npt']:
          lines += " {0:} {1:}".format(vt,v['flavour'])
        elif vt is 'nst' and v['flavour'] in flavour['nst']:
          lines += " {0:} {1:}".format(vt,v['flavour'])
        else:
          print("unknown combination {0:} {1:}, read the manual".format(vt,flavour[vt]))
      return lines+"\n"   

    def addprint(self,key):
        line = "{0:s} {1:}\n".format(key,self.d[key])
        line += "print {}\n".format(key)
        return line

    def addattrib(self,key,at):
        line = "{0:s} {1:} {2:}\n".format(key,at,self.d[key])
        return line

    def write(self,filename="CONTROL"): 
        ctrl = self.adddire('title',nokey=True) 

        for key,val in self.d.items():
          if key not in ['finish','title']:
              if key =='ensemble': 
                ctrl += self.addensemble(val)
              elif key in ['rdf','analysis','vaf','zden']:
                ctrl += self.addprint(key)
              elif key in ['job','close']:
                  ctrl += self.addattrib(key,'time')
              elif key == 'io':
                ctrl += self.addio(val)
              elif key == 'variable':
                x=0
              elif key == 'timestep':
                ctrl += self.addtimestep(val)
              else:
                ctrl += self.adddire(key)

        ctrl += self.adddire('finish',nokey=True) 
        with open(filename,"w") as f:
          f.write(ctrl)

