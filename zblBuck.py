#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

__author__ = "Alin Marin Elena <alin@elena.space>"
__copyright__ = "Copyright© 2017 Alin M Elena"
__license__ = "MIT"
__version__ = "1.0"

from math import *

zk=[0.18175,0.50986,0.28022,0.02817]
zke=[3.1998,0.94229,0.40290,0.20162]
a0= 0.52917721067
#e2=138935.4835e0
e2=14.39942



def ddphi(r,a):
    return sum([zk[i]*zke[i]*zke[i]*exp(-zke[i]*r/a)/(a*a) for i in range(4)] )

def dphi(r,a):
    return sum([-zk[i]*zke[i]*exp(-zke[i]*r/a)/a for i in range(4)] )

def phi(r,a):
    return sum([ zk[i]*exp(-zke[i]*r/a) for i in range(4)])

def V(r,zi,zj):
    a=0.88534*a0/(zi**0.23+zj**0.23)
    return zi*zj*phi(r,a)*e2/r

def dV(r,zi,zj):
    a=0.88534*a0/(zi**0.23+zj**0.23)
    return -V(r,zi,zj)/r+zi*zj*dphi(r,a)*e2/r

def ddV(r,zi,zj):
    a=0.88534*a0/(zi**0.23+zj**0.23)
    return -dV(r,zi,zj)/r+V(r,zi,zj)/r**2+zi*zj*ddphi(r,a)*e2/r-zi*zj*dphi(r,a)*e2/(r*r)

#U(r)=a*exp(-r/ρ)-c/r**6
def U(r,a,ρ,c):
  return a*exp(-r/ρ)-c/r**6

#G(r)=-r*∂U/∂r
def G(r,a,ρ,c):
    return r*a/ρ*exp(-r/ρ)-6.0*c/r**6

#dU(r)=∂U/∂r
def dU(r,a,ρ,c):
    return -a/ρ*exp(-r/ρ)+6.0*c/r**7

#ddU(r)=∂^2U/∂r^2
def ddU(r,a,ρ,c):
    return a/(ρ*ρ)*exp(-r/ρ)-42.0*c/r**8

#elrc=∫_rcut^∞U(r)r^2dr
def Elrc(rc,a,ρ,c):
    return a*ρ*(rc**2+2.0*ρ*rc+2.0*ρ**2)*exp(-rc/ρ)-c/(3.0*rc**3)

#vlrc=∫_rcut^∞ r*∂U/∂r*r^2dr
def Vlrc(rc,a,ρ,c):
    return -a*(rc**3+3.0*ρ*rc**2+6.0*ρ**2*rc+6.0*ρ**3)*exp(-rc/ρ)+2.0*c/rc**3

def espline(x,b):
    return exp(b[0]*x**5+b[1]*x**4+b[2]*x**3+b[3]*x**2+b[4]*x+b[5])

def despline(x,b):
    return (5*b[0]*x**4+4*b[1]*x**3+3*b[2]*x**2+2*b[3]*x+b[4])*espline(x,b)

def ZSB(r,zi,zj,r1,r2,b,a,ρ,c):
    if r<r1:
       return V(r,zi,zj)
    elif r<r2:
        return espline(r,b)
    else:
        return U(r,a,ρ,c)

def dZSB(r,zi,zj,r1,r2,b,a,ρ,c):
    if r<r1:
       return dV(r,zi,zj)
    elif r<r2:
        return despline(r,b)
    else:
        return dU(r,a,ρ,c)

def ZBLSplineBuck(zi,zj,r1,r2,b,a,ρ,c):
  AA1=V(r1,zi,zj)
  AA2=U(r2,a,ρ,c)
  BB1=dV(r1,zi,zj)
  BB2=dU(r2,a,ρ,c)
  CC1=ddV(r1,zi,zj)
  CC2=ddU(r2,a,ρ,c)
  # avoid meaningless log
  # by uniformly shifting all the points
  s=0
  if AA1<=0.0 or AA2<=0.0:
      s=1.0-min(AA1,AA2)

  A1=log(AA1+s)
  A2=log(AA2+s)
  B1=BB1/AA1
  B2=BB2/AA2
  C1=CC1/AA1-BB1**2/AA1**2
  C2=CC2/AA2-BB2**2/AA2**2

  K=-2*r2**5+10*r1*r2**4-20*r1**2*r2**3+20*r1**3*r2**2-10*r1**4*r2+2*r1**5
  b[0]=((C1-C2)*r2**2+r1*((2*C2-2*C1)*r2-6*B2-6*B1)+6*(B2+B1)*r2+(C1-C2)*r1**2-12*A2+12*A1)/K
  b[1]=-((3*C1-2*C2)*r2**3+r1*((C2-4*C1)*r2**2+(2*B2-2*B1)*r2-30*A2+30*A1)+(14*B2+16*B1)*r2**2+r1**2*((4*C2-C1)*r2-16*B2-14*B1)-30*A2*r2+30*A1*r2+(2*C1-3*C2)*r1**3)/K
  b[2]=((3*C1-C2)*r2**4+r1*((-4*C2*r2**3)+(32*B2+28*B1)*r2**2-80*A2*r2+80*A1*r2)+(8*B2+12*B1)*r2**3+r1**2*((8*C2-8*C1)*r2**2+((-28*B2)-32*B1)*r2-20*A2+20*A1)-20*A2*r2**2+20*A1*r2**2+r1**3*(4*C1*r2-12*B2-8*B1)+(C1-3*C2)*r1**4)/K
  b[3]=((-C1*r2**5)+r1*((3*C2-4*C1)*r2**4+((-24*B2)-36*B1)*r2**3+60*A2*r2**2-60*A1*r2**2)+r1**2*(8*C1*r2**3+(12*B1-12*B2)*r2**2+60*A2*r2-60*A1*r2)+r1**3*((36*B2+24*B1)*r2-8*C2*r2**2)+(4*C2-3*C1)*r1**4*r2+C2*r1**5)/K
  b[4]=-(r1*((-2*C1*r2**5)-10*B1*r2**4)+2*B1*r2**5+r1**2*((3*C2+C1)*r2**4+((-24*B2)-16*B1)*r2**3+60*A2*r2**2-60*A1*r2**2)+r1**3*((4*C1-4*C2)*r2**3+(16*B2+24*B1)*r2**2)+r1**4*(((-C2)-3*C1)*r2**2+10*B2*r2)+r1**5*(2*C2*r2-2*B2))/K
  b[5]=(r1**2*((-C1*r2**5)-10*B1*r2**4-20*A1*r2**3)+r1*(2*B1*r2**5+10*A1*r2**4)-2*A1*r2**5+r1**3*((C2+2*C1)*r2**4+(8*B1-8*B2)*r2**3+20*A2*r2**2)+r1**4*(((-2*C2)-C1)*r2**3+10*B2*r2**2-10*A2*r2)+r1**5*(C2*r2**2-2*B2*r2+2*A2))/K
  b[5]=b[5]-s


a=[]
ρ=[]
c=[]
z={}
p=[]
r1=[]
r2=[]
with open("pot.dat") as f:
    for line in f.readlines():
        el1,el2,z1,z2,la,rho,lc,lr1,lr2 = line.split()
        a.append(float(la))
        ρ.append(float(rho))
        c.append(float(lc))
        z[el1]=int(z1)
        z[el2]=int(z2)
        r1.append(float(lr1))
        r2.append(float(lr2))
        p.append([el1,el2])

elrc=[0.0]*len(a)
vlrc=[0.0]*len(a)
b=[[0,0,0,0,0,0]]*len(a)

ngrid=1504
rcut=15.0


for  i in range(len(a)):
  print(p[i][0],p[i][1])
  ZBLSplineBuck(z[p[i][0]],z[p[i][1]],r1[i],r2[i],b[i],a[i],ρ[i],c[i])
  print(b[i])
#AA=np.array([
#        [r1**5,r1**4,r1**3,r1**2,r1,1],
#        [r2**5,r2**4,r2**3,r2**2,r2,1],
#        [5*r1**4,4*r1**3,3*r1**2,2*r1,1,0],
#        [5*r2**4,4*r2**3,3*r2**2,2*r2,1,0],
#        [20*r1**3,12*r1**2,6*r1,2,0,0],
#        [20*r2**3,12*r2**2,6*r2,2,0,0]
#        ])
#BB=np.array([A1,A2,B1,B2,C1,C2])
#b=np.linalg.solve(AA,BB)
delpot=rcut/(ngrid-4)


f=open("TABLE","w")
f.write("{0:72s}\n".format('table for interaction'))
f.write("{0:20.10f}{1:20.10f}{2:10d}\n".format(delpot,rcut,ngrid))

for i in range(len(a)):
  elrc[i]=Elrc(rcut,a[i],ρ[i],c[i])
  vlrc[i]=Vlrc(rcut,a[i],ρ[i],c[i])

  f1=open("zblbuck{0:s}-{1:s}.dat".format(p[i][0],p[i][1]),"w")

  U01=[U(delpot*j,a[i],ρ[i],c[i]) for j in range(1,ngrid+1,1) ]
  V01=[V(j*delpot,z[p[i][0]],z[p[i][1]]) for j in range(1,ngrid+1,1)  ] 
  dU01=[dU(delpot*j,a[i],ρ[i],c[i]) for j in range(1,ngrid+1,1) ]
  dV01=[dV(j*delpot,z[p[i][0]],z[p[i][1]]) for j in range(1,ngrid+1,1)  ] 
  N01=[ZSB(j*delpot,z[p[i][0]],z[p[i][1]],r1[i],r2[i],b[i],a[i],ρ[i],c[i]) for j in range(1,ngrid+1,1)  ] 
  dN01=[-j*delpot*dZSB(j*delpot,z[p[i][0]],z[p[i][1]],r1[i],r2[i],b[i],a[i],ρ[i],c[i]) for j in range(1,ngrid+1,1)  ] 
  f1.write("#{0:17s} {1:17s} {2:17s} {3:17s} {4:17s} {5:17s} {6:17s} {7:17s} {8:17s}\n".format('r[A]','Buck[eV]','dBuck','ZBL[eV]','dZBL','Z+S+B[eV]','dZSB','deltaBuck','deltaZBL'))
  for j in range(0,ngrid,1):
    f1.write("{0:17.12e} {1:17.12e} {2:17.12e} {3:17.12e} {4:17.12e} {5:17.12e} {6:17.12e} {7:17.12e} {8:17.12e}\n".format((j+1)*delpot,U01[j],dU01[j],V01[j],dV01[j],N01[j],dN01[j],U01[j]-N01[j],V01[j]-N01[j]))
  f1.close()

  f.write("{0:8s}{1:8s}{2:20.10f}{3:20.10f}\n".format(p[i][0],p[i][1],elrc[i],vlrc[i]))
  for j in range((ngrid+3)//4):
    sx=4*j
    nx=min(4*j+4,ngrid)
    f.write("{0:17.12e} {1:17.12e} {2:17.12e} {3:17.12e}\n".format(*N01[sx:nx]))
  for j in range((ngrid+3)//4):
    sx=4*j
    nx=min(4*j+4,ngrid)
    f.write("{0:17.12e} {1:17.12e} {2:17.12e} {3:17.12e}\n".format(*dN01[sx:nx]))
f.close()
