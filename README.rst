this is a collection of scripts written a lot of times as proof of concept which I hope to be useful to others.
there is no promise of support of any kind so use at your own risk. That being said if you find any bug feel free to add an issue
and you never know.

requirements
============

#. python 3
#. numpy and friends 
#. matplotlib   
#. pyqt5 above 5.6 

tools
=====

#. potential.py creates a TABLE file from a potential contains all the ingredients for success
#. statis.py reads a STATIS file and plots it, also has the option to flatten it in columns
#. control.py small gui to create a control file. It does not read an existing file 
#. jupyter folder, contains simple jupyter notebooks example and a module to help read various dlpoly files and return them as numpy
   arrays   
