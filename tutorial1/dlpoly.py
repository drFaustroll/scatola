#!/usr/bin/env python3 
# -*- coding: UTF-8 -*-

__author__ = "Alin Marin Elena <alin@elena.space>"
__copyright__ = "Copyright© 2018-2019 Alin M Elena"
__license__ = "GPL-3.0-only"
__version__ = "1.0"

import numpy as np
import shlex as lex
import subprocess  as sp
import control as ctrl

control=ctrl.control()

def readStatis(filename="STATIS"):

  try:
    h1, h2, s = open(filename).read().split('\n',2)
  except IOError:
    return 0,[]

  d = np.array(s.split(), dtype=float)
  nd = int(d[2])
  n = d.size//(nd+3)
  d.shape = n, nd+3
  datumNames = [ "step", "time","ignore",
             "1-1 total extended system energy",
             "1-2 system temperature",
             "1-3 configurational energy",
             "1-4 short range potential energy",
             "1-5 electrostatic energy",
             "2-1 chemical bond energy",
             "2-2 valence angle and 3-body potential energy",
             "2-3 dihedral, inversion, and 4-body potential energy",
             "2-4 tethering energy",
             "2-5 enthalpy (total energy + PV)",
             "3-1 rotational temperature",
             "3-2 total virial",
             "3-3 short-range virial",
             "3-4 electrostatic virial",
             "3-5 bond virial",
             "4-1 valence angle and 3-body virial",
             "4-2 constraint bond virial",
             "4-3 tethering virial",
             "4-4 volume",
             "4-5 core-shell temperature",
             "5-1 core-shell potential energy",
             "5-2 core-shell virial",
             "5-3 MD cell angle α",
             "5-4 MD cell angle β",
             "5-5 MD cell angle γ",
             "6-1 PMF constraint virial",
             "6-2 pressure",
             "6-3 exdof"]

  for i in range(28,nd):
      datumNames.append("{0:d}-{1:d} col_{2:d}".format(i//5+1,i%5+1,i+1))
  print("legend/map see manual for complete meanings: ")
  for i in range(nd):
      print(i,datumNames[i])

  return d,datumNames

def readRDF(filename="RDFDAT"):

    try:
      title, header, rdfall = open(filename).read().split('\n',2)
    except IOError:
        return 0,[]

    nrdf,npoints=map(int, header.split())
    b=2*(npoints+1)
    d=np.zeros((nrdf+1,npoints,2),dtype=float)
    labels=[]
    s=rdfall.split()
    for i in range(nrdf):
      x=s[b*i:b*(i+1)]
      y=np.array(x[2:],dtype=float)
      y.shape= npoints,2
      d[i,:,:]=y
      labels.append(x[0]+" ... "+x[1])
    print("legend/map of rdf returned data: ")
    for i in range(nrdf):
      print(i,": ",labels[i])
    return d,labels

def readConfig(filename="CONFIG"):

    try:
      f=open(filename,'r')
    except IOError:
      return []

    line = f.readline()
    line = f.readline().split()
    levcfg = int(line[0])
    imcon = int(line[1])
    pbc = False
    if imcon > 0:
        pbc = True
    cell = np.zeros((3, 3))
    if pbc:
        for j in range(3):
            line = f.readline().split()
            for i in range(3):
                try:
                    cell[j, i] = float(line[i])
                except ValueError:
                    raise RuntimeError("error reading cell")
    symbols = []
    positions = []
    velocities = []
    forces = []
    line = f.readline()
    while line:
        symbol = line.split()[0]
        symbols.append(symbol)
        x, y, z = f.readline().split()[:3]
        positions.append([float(x), float(y), float(z)])
        if levcfg > 0:
            vx, vy, vz = f.readline().split()[:3]
            velocities.append([float(vx), float(vy), float(vz)])
        if levcfg > 1:
            fx, fy, fz = f.readline().split()[:3]
            forces.append([float(fx), float(fy), float(fz)])
        line = f.readline()
    return symbols,cell,np.array(positions,dtype=float),np.array(velocities,dtype=float),np.array(forces,dtype=float),pbc

def run(dlp="DLPOLY.Z", control="CONTROL",modules=[],np=1,mpi='mpirun -n'):

  #this is very primitive one shall check the existence of files...
  # even alter control parameters

 if np>1:
     cdlp = "{0:s} {1:d} {2:s} {3:s}".format(mpi,np,dlp,control)
 else:
     cdlp = "{0:s} {1:s}".format(dlp,control)

 if modules:
     ll="module purge && module load " + modules
     with open("env.sh",'w') as f:
       f.write(ll+"\n")
       f.write(cdlp)
     cmd = ['sh ./env.sh']
 else:
     cmd = [cdlp]

 sp.run(cmd,shell=True,check=False,stderr=sp.STDOUT)

