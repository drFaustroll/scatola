#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

__author__ = "Alin Marin Elena <alin@elena.space>"
__copyright__ = "Copyright© 2018-2019 Alin M Elena"
__license__ = "GPL-3.0-only"
__version__ = "1.0"

from copy import deepcopy as copy
import collections
import six

# python 3.8+ compatibility
try:
    collectionsAbc = collections.abc
except:
    collectionsAbc = collections


dlp_tokens = dict(temperature = 100.0,finish = 'finish',title='no title',io={},ensemble={},pressure=0.0,collect=False,\
        steps=10,equilibration=5,print=1,stats=1,cutoff=0.0,variable=False,timestep=0.001,job=1000,close=100,\
        traj={},rdf=10,l_scr=False,scale=10,rpad=1.0,rvdw=10.0,ewald={})



def update(d, u):
  for k, v in six.iteritems(u):
    dv = d.get(k, {})
    if not isinstance(dv, collectionsAbc.Mapping):
        d[k] = v
    elif isinstance(v, collectionsAbc.Mapping):
        d[k] = update(dv, v)
    else:
        d[k] = v
  return d

def checktokens(x):
  for key, val in x.items():
    if key in dlp_tokens:
      assert type(val) == type(dlp_tokens[key]), key + " has the wrong type"
    else:
        print("key {:s} is unknown check the manual".format(key))

class control(object):

    def __init__(self, **d):
      self.d={}
      self.d['finish']='finish'
      self.d['io']=dict(field='FIELD',config='CONFIG',statis='STATIS')
      self.d['title']='no title'
      self.d['ensemble']=dict(type='nve',flavour='langevin',aniso='',f=0.5,f1=0.5,f2=0.5,f3=0.5,gamma=0.5,semi=False)
      self.d['temperature']=300.0
      self.d['pressure']=0.0
      self.d['cutoff']=8.0
      self.d['timestep']=0.001
      self.d['job']=1000
      self.d['close']=100
      self.d['collect']=True
      self.d['variable']=False
      self.d['traj']=dict(switch=False,start=0,freq=100,level=0)

      update(self.d,d)
      checktokens(d)

    def set(self, **nd):
       update(self.d,nd)
       checktokens(nd)

    def directives(self):
      return self.d.keys()

    def adddire(self,key,nokey=False):
      if nokey:
        line = "{0:}\n".format(self.d[key])
      else:
        line = "{0:} {1:}\n".format(key,self.d[key])
      return line
    
    def adddirelogical(self,key):
      
      line = "{0:}\n".format(key)
      return line


    def addtraj(self,v):
        line = ''
        if v['switch'] :
          line ="traj {0:} {1:} {2:}\n".format(v['start'],v['freq'],v['level'])
        return line

    def addio(self,v):
        lines=""
        for key,val in v.items():
            lines+="io {0:s} {1:}\n".format(key,val)
        return lines

    def addtimestep(self,v):

        lines = "timestep {0:}\n".format(v)
        if self.d['variable']:
            lines = "variable "+lines
        return lines

    def addewald(self,v):
        values = ['precision']
        line = "ewald "
        if 'precision' in v.keys():
          line += " {0:} {1:}".format('precision',v['precision'])
        return line+"\n"

    def addensemble(self,v):
      types=['nve','nvt','npt','nst']
      flavour={'nvt':['evans','berendsen','langevin','andersen','hoover','gst','ttm','inhomogenous','dpds1','dpds2'],\
           'npt':['langevin','berendsen','hoover','mtk'],\
           'nst':['langevin','berendsen','hoover','mtk']}
      vt=v['type']
      aniso=['area','tens','orth','']

      lines="ensemble"
      if vt not in types:
          print("Unsupported thermostat type, {0:}, read the manual".format(vt))
      else:
        if vt is 'nve':
          lines += " {0:}".format(vt)
        elif vt == 'nvt' and (v['flavour'] in flavour['nvt']):
          lines += " {0:} {1:}".format(vt,v['flavour'])
        elif vt == 'npt' and v['flavour'] in flavour['npt']:
          lines += " {0:} {1:} {2:} {3:} ".format(vt,v['flavour'],v['f1'],v['f2'])
        elif vt == 'nst' and v['flavour'] in flavour['nst']:
          lines += " {0:} {1:} {2:} {3:} ".format(vt,v['flavour'],v['f1'],v['f2'])
        else:
          print("unknown combination {0:} {1:}, read the manual".format(vt,flavour[vt]))

      if vt == 'nvt':
        if v['flavour'] in ['langevin','hoover','berendsen']:
          lines += " {0:} ".format(v['f'])
        elif v['flavour'] in ['andersen','gst']:
          lines += " {0:} {1:} ".format(v['f1'],v['f2'])
        elif v['flavour'] in ['ttm','inhomogenous']:
          lines += " {0:} {1:} {2:} ".format(v['f1'],v['f2'],v['f3'])
        elif v['flavour'] in ['dpds1','dpds2']:
          lines += " {0:} ".format(v['gamma']) 
      if vt == 'nst' and v['aniso'] != '':
          lines += " {0:} ".format(v['aniso'])
          if v['aniso'] == "tens":
            lines += " {0:} ".format(v['gamma'])
          if v['semi']:
              lines += " semi" 
      return lines+"\n"   

    def addprint(self,key):
        line = "{0:s} {1:}\n".format(key,self.d[key])
        line += "print {}\n".format(key)
        return line

    def addattrib(self,key,at):
        line = "{0:s} {1:} {2:}\n".format(key,at,self.d[key])
        return line

    def write(self,filename="CONTROL"): 
        ctrl = self.adddire('title',nokey=True) 

        for key,val in self.d.items():
          if key not in ['finish','title']:
              if key =='ensemble': 
                ctrl += self.addensemble(val)
              elif key in ['rdf','analysis','vaf','zden']:
                ctrl += self.addprint(key)
              elif key in ['job','close']:
                ctrl += self.addattrib(key,'time')
              elif key == 'io':
                ctrl += self.addio(val)
              elif key == 'variable':
                x=0
              elif key == 'timestep':
                ctrl += self.addtimestep(val)
              elif key in ['collect','l_scr']:
                ctrl +=  self.adddirelogical(key)
              elif key == 'traj':
                ctrl += self.addtraj(val)
              elif key =='ewald': 
                ctrl += self.addewald(val)
              else:
                ctrl += self.adddire(key)

        ctrl += self.adddire('finish',nokey=True) 
        with open(filename,"w") as f:
          f.write(ctrl)

