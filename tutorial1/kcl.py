#!/usr/bin/env python3

import dlpoly as dlp


dlp.control.set(title="Potassium chloride")
dlp.control.set(io=dict(field="kcl.field",config="kcl.config"),temperature=300.0,steps=2000,equilibration=500,print=10,stats=10,collect=True)
dlp.control.set(pressure=100.0,timestep=0.005,cutoff=7.0,rpad=0.25,scale=10)
dlp.control.set(ensemble=dict(type='nst',flavour='hoover',f1=0.1,f2=1.0))
dlp.control.set(rdf=10)
dlp.control.set(ewald=dict(precision=1.0e-6))
dlp.control.write(filename="kcl.control")
dlp.run(dlp="DLPOLY.Z",control="kcl.control")
